# GDPR Documents

The aime of this project is to read data from i-doit PRO with installed Data Privacy Add-on and generate documents to make this information available to the supervisory authority on request.

## Usage

The scripts need `pandoc` installed on the system. 
 
Rename the file `vendor/idoit/Config.php.dist` to `vendor/idoit/Config.php` and alter the settings according to your environment.

You can call the script `all_processings.sh` from the commandline to generate `LaTex/all_processings.tex`. From this file, a functioning LaTex distribution assumed, you may create a pdf file e.g.  `pdflatex -file-line-error -interaction=nonstopmode -synctex=1 LaTex/all_processings.tex`. 
