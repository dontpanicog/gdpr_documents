<?php
/**
 * Created don’t panic it-services og.
 * User: chris
 * Date: 14.12.17
 * Time: 21:12
 */
require_once 'vendor/idoit/Config.php';
require_once 'vendor/autoload.php';
use bheisig\idoitapi\API;
use bheisig\idoitapi\CMDBObjects;
use bheisig\idoitapi\CMDBCategory;

$file     = 'Markdown/all_processings.md';
$fcontent = '';

$api = new API([
    'url' => $settings['idoit_url'],
    'key' => $settings['idoit_key'],
    'username' => $settings['idoit_username'],
    'password' => $settings['idoit_password']
]);


$api->login();

// Lese alle Objekte des Typs Verarbeitung
$cmdbObjects = new CMDBObjects($api);
$objects = $cmdbObjects->read(['type' => 'C__OBJTYPE__PRIVACY_PROCESSING']);



foreach ($objects as $object) {
    $fcontent .= '# ' . $object['title']. PHP_EOL . PHP_EOL;
    $category = new CMDBCategory($api);
    $result_angaben_zum_verfahren = $category->read($object['id'], 'C__CATG__CUSTOM_FIELDS_ANGABENZUMVERFAHREN');

    $fcontent .= '## Zweck(e) der Verarbeitungstätigkeit '. PHP_EOL . PHP_EOL;
    $fcontent .= html_entity_decode($result_angaben_zum_verfahren[0]["f_wysiwyg_c_1501762461759"]). PHP_EOL . PHP_EOL;
    $fcontent .= '## Aufbewahrungsfrist '. PHP_EOL . PHP_EOL;
    $fcontent .= html_entity_decode($result_angaben_zum_verfahren[0]["f_wysiwyg_c_1501762492910"]). PHP_EOL . PHP_EOL;

    // Zugewiesene Kontakte C__CATG__CONTACT
    $result_contact_assignment = $category->read($object['id'], 'C__CATG__CONTACT');

    foreach ($result_contact_assignment as $contact_assignment) {

        if ($contact_assignment['role']['title'] == 'LC__MODULE__PRIVACY__ROLE__CONTROLLER') {
            $fcontent .= '## Verantwortlicher' . PHP_EOL . PHP_EOL;
        } elseif ($contact_assignment['role']['title'] == 'LC__MODULE__PRIVACY__ROLE__PROCESSOR') {
            $fcontent .= '## Auftragsverarbeiter' . PHP_EOL . PHP_EOL;
        } else {
            $fcontent .= '## ' . $contact_assignment['role']['title'] . PHP_EOL . PHP_EOL;
        }


        // Adressen der Kontakte

        $contact_address = $category->read($contact_assignment['contact'][0]['id'], 'C__CATG__ADDRESS');
        foreach ($contact_address as $contact_address) {
            $fcontent .= $contact_address['street'] . ' ' . $contact_address['house_no'] . '  ' . PHP_EOL;
            $fcontent .= $contact_address['postcode'] . ' ' . $contact_address['city'] . PHP_EOL;
        }

        $fcontent .= PHP_EOL;

    }

    // Zugewiesene Datenkategorien C__CATG__CUSTOM_FIELDS_DATENKATEGORIEZUWEISUNG
    $result_zugewiesene_datenkatekorien = $category->read($object['id'], 'C__CATG__CUSTOM_FIELDS_DATENKATEGORIEZUWEISUNG');
    if (count($result_zugewiesene_datenkatekorien)) {
        $fcontent .= '## Verarbeitete Datenkategorien und betroffene Personen' . PHP_EOL . PHP_EOL;

        $fcontent .= '| Betroffene Personen | Datenkategorie | Übermittlungen |' . PHP_EOL;
        $fcontent .= '| - | - | - |' . PHP_EOL;

        $rechtsgrundlagen  = array();

        foreach ($result_zugewiesene_datenkatekorien as $zugewiesene_datenkategorien) {
            //var_dump($zugewiesene_datenkategorien['f_popup_c_1503580877383']);
            if ($zugewiesene_datenkategorien['f_text_c_1503569234510'] == '' || !$zugewiesene_datenkategorien['f_text_c_1503569234510']) continue; //überspinge leere Einträge
            $fcontent .= '| ' . $zugewiesene_datenkategorien['f_popup_c_1503563013173']['title'] . ' | ' . $zugewiesene_datenkategorien['f_text_c_1503569234510'] . ' | ' . data_transfers($zugewiesene_datenkategorien['f_popup_c_1503580877383']) . ' |' . PHP_EOL;
            $fcontent .= '|  |  |  |  ' . PHP_EOL;

            // Sammeln gleicher Rechtsgrundlagen
            if (isset($zugewiesene_datenkategorien['f_popup_c_1501769914393']['id'])) {
                $rechtsgrundlagen[$zugewiesene_datenkategorien['f_popup_c_1501769914393']['id']] = '- ' . $zugewiesene_datenkategorien['f_popup_c_1501769914393']['title'];
            }

        }

        $fcontent .= PHP_EOL;

        $fcontent .= '## Rechtsgrundlagen für die Verarbeitung' . PHP_EOL;
        $fcontent .= implode( PHP_EOL, $rechtsgrundlagen);

        $fcontent .= PHP_EOL . PHP_EOL;
    }

}



file_put_contents($file, $fcontent);


exec('cat Templates/records_of_processings.tex > LaTex/all_processings.tex && pandoc Markdown/all_processings.md -t latex >> LaTex/all_processings.tex && cat Templates/eod.tex >> LaTex/all_processings.tex');

$file = 'LaTex/all_processings.tex';

if (PHP_SAPI === 'cli') {
    if (file_exists($file)) {
        echo 'generated file ' . $file . PHP_EOL;
    }
} else {
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/x-latex');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }
}

function data_transfers($idoit_datatransfers) {
    $data_transfers = '';
    if (isset($idoit_datatransfers[0]) && is_array($idoit_datatransfers[0])) {
        foreach ($idoit_datatransfers as $idoit_datatransfer) {
            if (isset($idoit_datatransfer["title"])) $data_transfers .= $idoit_datatransfer["title"] . ', ';
        }
        trim($data_transfers, ' ,');
    } else {
        if (isset($idoit_datatransfers["title"])) $data_transfers = $idoit_datatransfers["title"];
    }

    return $data_transfers;
}

?>
